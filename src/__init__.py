# Minetest Web Gui is a webapp for managing a minetest server via a gui
# Copyright (C) 2020  SonoMichele (Michele Viotto)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from flask import Flask, render_template, Response, redirect, url_for
from shelljob import proc

from src.config import Config


SERVER_GROUP = proc.Group()
SERVER_PROCESS = None


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    from src.views import main, console
    app.register_blueprint(main.main)
    app.register_blueprint(console.console, url_prefix='/console')

    # this doesn't work
    # @app.route('/test')
    # def test():
    #     # this piece of code runs commands on the server console
    #     global SERVER_PROCESS
    #     SERVER_PROCESS.stdin.writelines("/kick SonoMichele\n")
    #     SERVER_PROCESS.stdin.flush()
    #     return redirect(url_for('main.home'))

    return app
